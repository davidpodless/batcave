﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BatCave.Terrain;

namespace BatCave {
/// <summary>
/// creates coins in the game.
/// </summary>
public class CoinsCreator : MonoSingleton<CoinsCreator> {
    public GameObject prefab;

    public override void Init() {
        Game.OnPointCreationEvent += OnPointCreation;
    }

    private void OnPointCreation(TerrainGenerator.TerrainPoint point) {
        int chance = Random.Range(1,3);

        if(chance == 1) {
            float yPos = Random.Range(point.floorY, point.ceilingY);
            Instantiate(prefab, new Vector3(point.x, yPos, 0), new Quaternion());
        }
    }
}
}