﻿using UnityEngine;
using BatCave.Terrain;

namespace BatCave {
/// <summary>
/// Allows calculating difficulty based on player success level.
/// </summary>
public class DifficultyManager : MonoSingleton<DifficultyManager> {
    public GameObject bat;

    static int hardnessCount = 0;   //how hard is it to the player
    static int curTopLevel = 3;     //the next "peek"
    static int level = 1;
    static int jump = 1;
    static int zeroLevelCounter = 5;

    public override void Init() {
        Game.OnPlayerPassedPointEvent += OnPlayerPassedPoint;
    }

    public static int GetNextDifficulty() {
        // Always return 0 until the game starts.
        if (!Game.instance.HasStarted) return 0;

        if (zeroLevelCounter < 5) {
            zeroLevelCounter++;
            return 0;
        }

        int nextLevel = level;
        level += jump;
        if (level >= curTopLevel) {
            //need to have a new peek:
            curTopLevel = Mathf.Min(curTopLevel + (int)Mathf.Ceil((float)curTopLevel / 3), 100);
            level = 0;
            jump = (int)Mathf.Ceil((float)curTopLevel / 4);  //we don't want every time the player do all the work again.
            zeroLevelCounter = 0;   //resting time!
        }
        Infra.DebugUtils.Log("going to level " + nextLevel + " and now top is: " + curTopLevel + " and jump is: " + jump);
        
        return nextLevel;
    }

    private void OnPlayerPassedPoint(TerrainGenerator.TerrainPoint point) {
        var passingSpace = new Vector2(point.ceilingY, point.floorY);
        var pos = bat.GetComponent<Transform>().position;
        if(((pos.y - passingSpace.y < 0.2) || (passingSpace.x - pos.y < 0.2)) && Mathf.Abs(pos.y - (passingSpace.y+ passingSpace.x))/2 > 0.1) {
            hardnessCount++;
            if(hardnessCount == 5) { 
                hardnessCount = 0;
                curTopLevel = (int)Mathf.Ceil(curTopLevel / 2);  //it's too hard, we make it easier
            }
        }
        bat.GetComponent<Bat>().xSpeed = Mathf.Max(2 + (0.02f * level), bat.GetComponent<Bat>().xSpeed);   //as it gets hard, it gets faster!
    }
}
}
