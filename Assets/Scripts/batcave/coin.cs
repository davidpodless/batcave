﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BatCave {
public class coin : MonoBehaviour {
    private GameObject player;
    private GameObject game;
    private GameObject sound;

    private int pointsVal = 10;
    public void Start() {
        player = GameObject.Find("Bat");
        game = GameObject.Find("Game");
        sound = GameObject.Find("coinColectSound");
        Terrain.TerrainAliveChecker.OnTerrainEnteredScreen += OnTerrainEnteredScreen;
    }
    
    //check if batmobile touches coin:
    void OnTriggerEnter2D(Collider2D other){
        if(other.gameObject == player.gameObject) {
            sound.GetComponent<AudioSource>().Play();
            game.GetComponent<Game>().addPoints(pointsVal);
            Destroy(gameObject);
        }
    }

    //delete the coinL
    private void OnTerrainEnteredScreen() {
        if (player.GetComponent<Transform>().position.x - GetComponent<Transform>().position.x > 5) {
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        Terrain.TerrainAliveChecker.OnTerrainEnteredScreen -= OnTerrainEnteredScreen;
    }
}
}