﻿using UnityEngine;

namespace BatCave.Spline {
    public class ContinuousSpline : SingleSpline {
        public ContinuousSpline(Vector2[] firstControlPoints) : base(firstControlPoints) {
            // Empty on purpose.
        }

        /// <summary>
        /// Continue the spline with more control points.
        /// </summary>
        public void AddControlPoints(params Vector2[] points) {
            // TODO: Implement: don't keep all of the control points, there could be
            //       millions of them!
            int oldPointLenght = this.points.Length;
            var lastPoint = this.points[oldPointLenght - 1];
            var oneEarlier = this.points[oldPointLenght - 2];
            this.points = new Vector2[points.Length + 2];
            this.points[0] = oneEarlier;
            this.points[1] = lastPoint;
            for (int i = 0; i < points.Length; i++) {
                this.points[2 + i] = points[i];
            }
            //create the same thing as in Single Spline, just now b_0 = 1. d_0 = k_n-1 (from the last time)
            // anything else - just like it was. it should works...
            int lastPointLocation = this.points.Length - 1;
            float[] aCalcVec = new float[this.points.Length];
            float[] bCalcVec = new float[this.points.Length];
            float[] cCalcVec = new float[this.points.Length];
            float[] dCalcVec = new float[this.points.Length];
            aCalcVec[0] = 0;
            bCalcVec[0] = 1;
            cCalcVec[0] = 0;
            dCalcVec[0] = kZero;
            for (int i = 1; i < lastPointLocation; i++) {
                var fromLastXPoint = this.points[i].x - this.points[i - 1].x;
                var toThisXPoint = this.points[i + 1].x - this.points[i].x;
                var fromLastYPoint = this.points[i].y - this.points[i - 1].y;
                var toThisYPoint = this.points[i + 1].y - this.points[i].y;
                aCalcVec[i] = 1 / fromLastXPoint;
                bCalcVec[i] = 2 * ((1 / fromLastXPoint) + (1 / toThisXPoint));
                cCalcVec[i] = 1 / toThisXPoint;
                dCalcVec[i] = 3 * ((fromLastYPoint / pow2(fromLastXPoint)) + (toThisYPoint / pow2(toThisXPoint)));
            }
            aCalcVec[lastPointLocation] = 1 / (this.points[lastPointLocation].x - this.points[lastPointLocation - 1].x);
            bCalcVec[lastPointLocation] = 2 / (this.points[lastPointLocation].x - this.points[lastPointLocation - 1].x);
            cCalcVec[lastPointLocation] = 0;
            dCalcVec[lastPointLocation] = 3 * ((this.points[lastPointLocation].y - this.points[lastPointLocation - 1].y) /
                            pow2(this.points[lastPointLocation].x - this.points[lastPointLocation - 1].x));

            float[] cApostropheVec = new float[cCalcVec.Length];
            float[] dApostropheVec = new float[dCalcVec.Length];
            for (int i = 0; i < cCalcVec.Length; i++) {
                cApostropheVec[i] = i == lastPointLocation ? 0 : cCalcVec[i] / (bCalcVec[i] - aCalcVec[i]);
                dApostropheVec[i] = i == 0 ? dCalcVec[0] / bCalcVec[0] :
                    (dCalcVec[i] - aCalcVec[i] * dApostropheVec[i - 1]) / (bCalcVec[i] - aCalcVec[i] * cApostropheVec[i - 1]);

            }
            float[] kVec = new float[dApostropheVec.Length];
            for (int i = kVec.Length - 1; i >= 0; i--) {    //finding all the k's
                kVec[i] = i == kVec.Length - 1 ? dApostropheVec[i] : dApostropheVec[i] - cApostropheVec[i] * kVec[i + 1];
            }
            aVec = new float[kVec.Length];
            bVec = new float[kVec.Length];
            aVec[0] = 0;
            bVec[0] = 0;
            for (int i = 1; i < aVec.Length; i++) {     //finding all the a's and b's
                aVec[i] = kVec[i - 1] * (this.points[i].x - this.points[i - 1].x) - (this.points[i].y - this.points[i - 1].y);
                bVec[i] = -kVec[i] * (this.points[i].x - this.points[i - 1].x) + (this.points[i].y - this.points[i - 1].y);
            }
            kZero = kVec[kVec.Length - 2];
        }
    }
}
