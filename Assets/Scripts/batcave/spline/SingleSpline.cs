﻿using UnityEngine;

namespace BatCave.Spline {
    public class SingleSpline {
        protected float[] aVec;
        protected float[] bVec;
        protected float kZero;
        protected Vector2[] points;
        public SingleSpline(Vector2[] controlPoints) {
            points = controlPoints;
            int lastPointLocation = points.Length - 1;
            float[] aCalcVec = new float[points.Length];
            float[] bCalcVec = new float[points.Length];
            float[] cCalcVec = new float[points.Length];
            float[] dCalcVec = new float[points.Length];
            aCalcVec[0] = 0;
            bCalcVec[0] = 2 / (points[1].x - points[0].x);
            cCalcVec[0] = 1 / (points[1].x - points[0].x);
            dCalcVec[0] = 3 * (points[1].y - points[0].y) / pow2(points[1].x - points[0].x);
            for (int i = 1; i < lastPointLocation; i++) {
                var fromLastXPoint = points[i].x - points[i - 1].x;
                var toThisXPoint = points[i + 1].x - points[i].x;
                var fromLastYPoint = points[i].y - points[i - 1].y;
                var toThisYPoint = points[i + 1].y - points[i].y;
                aCalcVec[i] = 1 / fromLastXPoint;
                bCalcVec[i] = 2 * ((1 / fromLastXPoint) + (1 / toThisXPoint));
                cCalcVec[i] = 1 / toThisXPoint;
                dCalcVec[i] = 3 * ((fromLastYPoint / pow2(fromLastXPoint)) + (toThisYPoint / pow2(toThisXPoint)));
            }
            aCalcVec[lastPointLocation] = 1 / (points[lastPointLocation].x - points[lastPointLocation - 1].x);
            bCalcVec[lastPointLocation] = 2 / (points[lastPointLocation].x - points[lastPointLocation - 1].x);
            cCalcVec[lastPointLocation] = 0;
            dCalcVec[lastPointLocation] = 3 * ((points[lastPointLocation].y - points[lastPointLocation - 1].y) /
                            pow2(points[lastPointLocation].x - points[lastPointLocation - 1].x));

            float[] cApostropheVec = new float[cCalcVec.Length];
            float[] dApostropheVec = new float[dCalcVec.Length];
            for (int i = 0; i < cCalcVec.Length; i++) {
                cApostropheVec[i] = i == lastPointLocation ? 0 : cCalcVec[i] / (bCalcVec[i] - aCalcVec[i]);
                dApostropheVec[i] = i == 0 ? dCalcVec[0] / bCalcVec[0] :
                    (dCalcVec[i] - aCalcVec[i] * dApostropheVec[i - 1]) / (bCalcVec[i] - aCalcVec[i] * cApostropheVec[i - 1]);

            }
            float[] kVec = new float[dApostropheVec.Length];
            for (int i = kVec.Length - 1; i >= 0; i--) {
                kVec[i] = i == kVec.Length - 1 ? dApostropheVec[i] : dApostropheVec[i] - cApostropheVec[i] * kVec[i + 1];
            }
            aVec = new float[kVec.Length]; // TODO - save last a,b from last run
            bVec = new float[kVec.Length];
            aVec[0] = 0;
            bVec[0] = 0;
            for (int i = 1; i < aVec.Length; i++) {
                aVec[i] = kVec[i - 1] * (points[i].x - points[i - 1].x) - (points[i].y - points[i - 1].y);
                bVec[i] = -kVec[i] * (points[i].x - points[i - 1].x) + (points[i].y - points[i - 1].y);
            }
            kZero = kVec[kVec.Length - 2];
        }

        /// <summary>
        /// Returns the value of the spline at point X.
        /// </summary>
        public float Value(float X) {
            // TODO: Implement: find the polynom f_i that passes at X and calculate
            //       f_i(X).
            int i = findLocation(X);
            if (i == -1)
                return 0; // TODO - what if can't find the point?
            float t = (X - points[i - 1].x) / (points[i].x - points[i - 1].x);
            return (1 - t) * points[i - 1].y + t * points[i].y + t * (1 - t) * (aVec[i] * (1 - t) + bVec[i] * t);
        }

        private int findLocation(float X) {
            int i = 1; // i cant be 0
            for (; i < points.Length; i++) {
                if (points[i].x >= X)
                    return i;
            }
            return -1;
        }

        protected float pow2(float x) {
            return Mathf.Pow(x, 2);
        }

    }
}