﻿using UnityEngine;
using Infra;

namespace BatCave.Terrain{
    public class ObstacleCreator : MonoSingleton<ObstacleCreator>{
        public static GameObject bat;

        private static bool isMiddleObstacle = false;
        private static int obstacle = 0;
        private static Vector2 lastPoint = new Vector2();   //x is floor, y is ceiling
        private static int difficulty;
        private static float minFloor = -1.8f;
        private static float maxCeiling = 1.8f;
        private static float height = 1.6f;
        private static int obsCounter = 0;
        private static float minGap = 1.2f;

        public static Vector2 obsNextPoint(int diff)
        {
            //Vector2 playerPos = bat.GetComponent<Transform>().position;

            if (!isMiddleObstacle)
            {
                obstacle = Random.Range(1, 4);
                // the obstacles are: 1- narrow tunnel
                //                    2- lunatic tunnel
                //                    3- upOrDown

                lastPoint = new Vector2();
                difficulty = diff;
                height = Mathf.Max((Random.Range(2f, 3f) / Mathf.Sqrt(difficulty)) + ((3/40) * Mathf.Sqrt(difficulty)), minGap);
            }
            
            switch(obstacle) {
                case 1:
                    return narrow();
                case 2:
                    return lunatic();
                default:
                    return upOrDown();
            }
        }

        private static Vector2 narrow() {
            DebugUtils.Log("narrow.");
            float floor = Random.Range(minFloor, maxCeiling - height);
            float ceil = floor + height;
            return new Vector2(floor, ceil);
        }


        private static Vector2 lunatic(){

            if (!isMiddleObstacle) {
                isMiddleObstacle = true;
            }

            float floor = Random.Range(minFloor, maxCeiling - height);
            while (Mathf.Abs(lastPoint.x - floor) > 1){  //we don't want it to have too sharp angle
                floor = Random.Range(minFloor, maxCeiling - height);
            }

            float ceil = floor + height;
            lastPoint.x = floor;
            lastPoint.y = ceil;

            obsCounter++; 
            if (obsCounter == 7) {
                obsCounter = 0;
                isMiddleObstacle = false;
            }
            return lastPoint;
        }


        private static Vector2 upOrDown(){
            if (!isMiddleObstacle){     //the start of this obs
                isMiddleObstacle = true;
                DebugUtils.Log("Enter to upOrDown");
            }

            float floorYPos = Random.Range(minFloor, maxCeiling - (height));

            //if this at the start or the end of the obs:
            float ceilYPos = Random.Range(floorYPos + height, maxCeiling);

            obsCounter++;
            if(obsCounter == 3) {   //this is the end :)
                obsCounter = 0;
                isMiddleObstacle = false;
                DebugUtils.Log("Finished upOrDown");
            }

            return obsCounter == 0 ? new Vector2(floorYPos, ceilYPos) : new Vector2(floorYPos, floorYPos + height);
        }
    }
}
